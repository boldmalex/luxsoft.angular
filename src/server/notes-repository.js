const { MongoClient } = require("mongodb");
const collectionName = "notes";

module.exports = class NotesRepository {
  constructor(url, dbName) {
    this.connection = MongoClient.connect(url);
    this.dbName = dbName;
  }

  getNotes() {
    console.log("request getNotes");
    return this.connection.then((client) =>
      client.db(this.dbName).collection(collectionName).find().toArray()
    );
  }

  getNoteById(id) {
    console.log("request getNoteById", id);
    return this.connection.then((client) =>
      client
        .db(this.dbName)
        .collection(collectionName)
        .find({ id: id })
        .toArray()
    );
  }

  removeNote(id) {
    console.log("request removeNote, id", id);
    return this.connection.then((client) =>
      client.db(this.dbName).collection(collectionName).deleteOne({ id: id })
    );
  }

  postNote(note) {
    console.log("request postNote:", note);
    return this.connection.then((client) =>
      client.db(this.dbName).collection(collectionName).insertOne(note)
    );
  }
};
