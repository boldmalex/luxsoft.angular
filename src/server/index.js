const express = require("express");
const session = require("express-session");
const path = require("path");
const bodyParser = require("body-parser");
const NotesRepository = require("./notes-repository");
const { nanoid } = require("nanoid");

const url = "mongodb://testUser:testUser@localhost/?maxPoolSize=40&w=majority";
const dbName = "notesDb";
const notesRepo = new NotesRepository(url, dbName);

var app = express();
app.use(express.static(path.join(__dirname, "../../dist/notes-app")));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

let nextId = 1;
const notes = [
  { id: nextId++, note: "Постороить дом" },
  { id: nextId++, note: "Посадить дерево" },
  { id: nextId++, note: "Отдыхать" },
  { id: nextId++, note: "Еще отдыхать" },
  { id: nextId++, note: "И еще" },
];

app.use(
  session({
    secret: "angular-test",
    resave: true,
    saveUninitialized: true,
  })
);

// Get notes
app.get("/notes", function (req, res) {
  //res.header("Access-Control-Allow-Origin", "*");
  //res.header("Access-Control-Allow-Headers", "Content-Type, X-RequestedWith");
  //console.log("current session", req.session.notes);

  notesRepo.getNotes().then(function (doc, err) {
    if (err) {
      res.status = 500;
      res.end();
    } else {
      res.send(doc);
    }
  });
});

// Post notes
app.post("/notes", function (req, res) {
  //res.header("Access-Control-Allow-Origin", "*");
  //res.header("Access-Control-Allow-Headers", "Content-Type, X-RequestedWith");
  //console.log("post note", req.body, req.body.text);

  const note = { id: nanoid(), note: req.body.text };
  notesRepo.postNote(note).then(function (postResult) {
    if (postResult.insertedId) {
      res.status = 201;
    } else {
      res.status = 500;
    }
    res.end();
  });
});

// Delete note
app.delete("/notes/:id", function (req, res) {
  //res.header("Access-Control-Allow-Origin", "*");
  //res.header("Access-Control-Allow-Headers", "Content-Type, X-RequestedWith");

  console.log("delete id", req.params.id);

  notesRepo.removeNote(req.params.id).then(function (delResult) {
    if (delResult.deletedCount > 0) {
      res.status = 200;
    } else {
      res.status = 404;
    }
    res.end();
  });
});

// Get note by id
app.get("/notes/:id", function (req, res) {
  //res.header("Access-Control-Allow-Origin", "*");
  //res.header("Access-Control-Allow-Headers", "Content-Type, X-RequestedWith");

  notesRepo.getNoteById(req.params.id).then(function (doc, err) {
    if (err) {
      res.status = 500;
      res.end();
    } else if (doc) {
      res.send(doc);
    } else {
      res.status = 404;
      res.end();
    }
  });
});

app.listen(3000);
