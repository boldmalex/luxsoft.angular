import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { NoteListComponent } from 'src/components/note-list/note-list.component';
import { AppComponent } from './app.component';
import { SectionsComponent } from 'src/components/sections/sections.component';



@NgModule({
  declarations: [	
    AppComponent,
    NoteListComponent,
    SectionsComponent
   ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
