import { Component } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  logs ="";
  section = "";
  
  setSection(section: string) {
    this.section = section;
  }

  addItem(newNote:string){
    this.logs += ` добавили заметку ${newNote}`;
  }
  
  delItem(delNote:string){
    this.logs += ` удалили заметку ${delNote}`;
  }
}
