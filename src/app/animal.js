"use strict";
var __decorate =
  (this && this.__decorate) ||
  function (decorators, target, key, desc) {
    var c = arguments.length,
      r =
        c < 3
          ? target
          : desc === null
          ? (desc = Object.getOwnPropertyDescriptor(target, key))
          : desc,
      d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function")
      r = Reflect.decorate(decorators, target, key, desc);
    else
      for (var i = decorators.length - 1; i >= 0; i--)
        if ((d = decorators[i]))
          r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
  };
Object.defineProperty(exports, "__esModule", { value: true });
exports.Animal = void 0;
function log(target, key, descriptor) {
  //const oldMethod = target[key];
  var oldMethod = descriptor.value;
  var newMethod = function () {
    var args = [];
    for (var _i = 0; _i < arguments.length; _i++) {
      args[_i] = arguments[_i];
    }
    console.log("entering method: ".concat(key, " (").concat(args, ")"));
    var result = oldMethod.apply(this, args);
    console.log("result: ".concat(key, " (").concat(result, ")"));
    return result;
  };
  descriptor.value = newMethod;
}
var Animal = /** @class */ (function () {
  function Animal(name, age, isHungry) {
    this.name = "";
    this.age = 0;
    this.isHungry = true;
    this.name = name;
    this.age = age;
    this.isHungry = isHungry;
  }
  Animal.defaultAnimal = function () {
    return new Animal("animal", 0, true);
  };
  Animal.prototype.eat = function () {
    return new Animal(this.name, this.age, (this.isHungry = false));
  };
  Animal.prototype.copy = function () {
    return Object.assign({}, this);
  };
  Animal.prototype.copy2 = function () {
    return Object.assign({}, this);
  };
  Animal.prototype.setAge = function (age) {
    return new Animal(this.name, age, (this.isHungry = false));
  };
  __decorate([log], Animal.prototype, "setAge", null);
  return Animal;
})();
exports.Animal = Animal;
var a = new Animal("test", 10, true);
var b = a.copy();
var c = a.copy2();
console.log(typeof a);
console.log(a.constructor);
console.log(a);
console.log("----------------");
console.log(typeof b);
console.log(b.constructor);
console.log(b);
console.log("----------------");
console.log(typeof c);
console.log(c.constructor);
console.log(c);
