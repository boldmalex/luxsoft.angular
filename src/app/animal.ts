import { Console } from "console";

function log(target:any, key:any, descriptor: any){
    //const oldMethod = target[key];
    const oldMethod = descriptor.value;

    const newMethod = function(...args){
        console.log(`entering method: ${key} (${args})`);
        const result = oldMethod.apply(this, args);
        console.log(`result: ${key} (${result})`);
        return result;
    }
    
    descriptor.value = newMethod;
}

interface iAnimal{
    new (name:string, age:number, isHungry: boolean) : Eat;
}

interface Copy {
    copy() : Animal 
}

interface Eat extends Copy{
    eat() : Animal;
}



export class Animal{
    name ="";
    age = 0;
    isHungry = true;

    constructor(name:string, age:number, isHungry:boolean){
        this.name = name;
        this.age = age;
        this.isHungry = isHungry;
    }

    static defaultAnimal(){
        return new Animal("animal", 0, true);
    }
    
    eat(): Animal{
        return  new Animal(this.name, this.age, this.isHungry = false);
    }

    copy(): Animal{
        return <Animal>Object.assign({}, this);
    }
    copy2(): Animal{
        return Object.assign({}, this) as Animal;
    }

    @log
    setAge(age:number): Animal{
        return  new Animal(this.name, age, this.isHungry = false);
    }
}

var a = new Animal("test", 10, true);
const b = a.copy();
const c = a.copy2();
console.log(typeof a);
console.log(a.constructor);
console.log(a);
console.log('----------------')
console.log(typeof b);
console.log(b.constructor);
console.log(b);
console.log('----------------')
console.log(typeof c);
console.log(c.constructor);
console.log(c);

