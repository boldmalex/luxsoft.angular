import { Injectable } from '@angular/core';
import { environment as env } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NotesServiceService {

constructor() { }

async loadAsync(){
  const url = `${env.serviceUrl}/notes`;
  const response = await fetch(url);
  if (response.ok) 
    return await response.json();
  else
    throw new Error(`get fetch error ${url}, received: ${response.status}`);
}

async postAsync(note:object){
  const url = `${env.serviceUrl}/notes`;
  console.log('Post Note: ', note, JSON.stringify(note));
  const response = await fetch(url, {
    method: "POST",
    body: JSON.stringify(note),
    headers: {
      "Content-Type": "application/json",
    },
  });
  if (!response.ok)
    throw new Error(`post fetch error ${url}, received: ${response.status}`);
}


async removeAsync(id:string) {
  const url = `${env.serviceUrl}/notes/${id}`;

  const response = await fetch(url, { method: "DELETE" });

  if (!response.ok)
    throw new Error(
      `delete fetch error ${url}, received: ${response.status}`
    );
}
}
