/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { NotesServiceService } from './NotesService.service';

describe('Service: NotesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NotesServiceService]
    });
  });

  it('should ...', inject([NotesServiceService], (service: NotesServiceService) => {
    expect(service).toBeTruthy();
  }));
});
