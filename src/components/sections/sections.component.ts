import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-sections',
  templateUrl: './sections.component.html',
  styleUrls: ['./sections.component.css']
})
export class SectionsComponent implements OnInit {
  
  sections: string[] = [
    'first',
    'last'
  ];
  activeSection="";

  constructor() { }

  ngOnInit() {
  }

  
  @Output() 
  sectionChanged: EventEmitter<string> = new EventEmitter<string>();
  
  showSection(section: string) {
    this.activeSection = section; 
    this.sectionChanged.emit(this.activeSection);
   }
}
