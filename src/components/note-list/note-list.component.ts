import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NotesServiceService } from '../NotesService.service';

@Component({
  selector: 'note-list',
  templateUrl: './note-list.component.html',
  styleUrls: ['./note-list.component.css']
})
export class NoteListComponent implements OnInit{
  title = 'Заметки';
  note = "";
  notes = [
    {id: '0', text: "probably error"}, 
  ];
   
  
  constructor(private service: NotesServiceService){
  }

  async ngOnInit(){
    await this.loadItems();
  }

  async loadItems(){
    var response = await this.service.loadAsync();
    this.notes = (response as Array<{id:string, note:string}>).map(item => ({id:item.id, text: item.note}));

    /*
    fetch(this.url)
    .then(response => response.json())
    .then(json => {
      this.notes = (json as Array<{id:number, title:string}>).map(item => ({id:item.id, text: item.title}))
    });
    */

    /*
    this.http.get<Array<{ id:number, title: string }>>(this.url)
      .toPromise()
      .then((notes) => {
        this.notes = notes?.map(line => ({id:item.id, text: item.title})) ?? [];
      })
     */
  }
 

  @Input() 
  section="";
  @Output()
  newNoteEvent = new EventEmitter<string>();
  @Output()
  delNoteEvent = new EventEmitter<string>();

  async inputNoteClick(note: string){
    await this.service.postAsync({text: note})
    this.note = "";
    await this.loadItems();
    this.newNoteEvent.emit(note);
  }

  async removeNoteClick(id: string){
    const note = this.notes.filter(n => n.id === id);
    if (note.length > 0){
      await this.service.removeAsync(id);
      console.log('removing id', id);
      await this.loadItems();
      this.delNoteEvent.emit(note[0].text);
    }
  }

  downNoteClick(i: number){
    if (i === this.notes.length - 1) 
      return;
    var element = this.notes[i];
    this.notes.splice(i, 1);
    this.notes.splice(i+1, 0, element)
  }

  upNoteClick(i: number){
    if (i === 0) 
      return;
    var element = this.notes[i];
    this.notes.splice(i, 1);
    this.notes.splice(i-1, 0, element)
  }
}

